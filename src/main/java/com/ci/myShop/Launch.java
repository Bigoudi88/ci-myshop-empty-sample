package com.ci.myShop;

import java.util.ArrayList;


import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class Launch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Item> itemsMarie = new ArrayList<Item>();
		
		Item itemA = new Item("Java pour les nulls", 12, (float) 2.99, 5);
		Item itemB = new Item("Tests Unitaires et crise sanitaire", 46, (float) 43.12, 17);
		
		itemsMarie.add(itemB);
		
		Storage storage = new Storage();
		

		ArrayList<Item> items = new ArrayList<Item>();	
		
		Item item1 = new Item("Harry Potter", 1, 55, 1);
		Item item2 = new Item("Game of Thrones", 2, 123, 1);
		
		items.add(item1);
		items.add(item2);
		Storage storageAurel =  new Storage();

		 
		for (Item item : items) {
			storageAurel.addItem(item);
		}
		
		Shop shop = new Shop(storage, 100);
		
		if (shop.buy(itemA)) {
			shop.buy(itemA);
			System.out.println("vous venez d'acheter : " + itemA.getName());
		}
		else {
			System.out.println("Vous n'avez pas les moyens d'acheter" + itemA.getName());
		}
	
		
		
		Shop monMagasinToutmignonShop = new Shop(storage, 100);
		
		monMagasinToutmignonShop.sell("Harry Potter");
		System.out.println("J'ai dans ma caisse dans mon magasin: " + monMagasinToutmignonShop.getCash());

	}

}
