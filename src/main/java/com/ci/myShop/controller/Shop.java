package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

public class Shop {
	
	private Storage storage;
	private float cash;

	
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.setCash(cash);
	}


	public Item sell(String Name) {
		if (this.storage.getItem(Name) != null) {
			Item item =	this.storage.getItem(Name);
			this.setCash(this.getCash() + item.getPrice());
			return item;
		}
		else {
			return null;
		}
	
	}


	public float getCash() {
		return cash;
	}


	public void setCash(float cash) {
		this.cash = cash;
	}
	
	
	
	public boolean buy(Item item) {
		
		if (this.getCash() >= item.getPrice()) {
			this.storage.addItem(item);
			this.setCash(getCash()-item.getPrice());
			return true;
		
	}
		else {
			
		return false;
	}
		
		
		
	}
}
