package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.ci.myShop.model.Item;

public class Storage {
 private Map<String, Item> itemMap;


public Storage() {
	super();
	itemMap = new HashMap<String, Item>();
}


public void addItem(Item obj) {
	itemMap.put(obj.getName(),obj);
}

public Item getItem(String name) {
	
	return (Item) itemMap.get(name);
}
//public int nbrItem(){
	// for each item, additionner le nbrElmt jusqu'� la fin du nombre de d'item
	// OU compter le nombre d'enregistrement item dans l'itemMap 
	//this.itemMap.get(itemMap);
	//return nbrItem;
//}


public  Map<String, Item> getItemMap() {
	
	return itemMap;
}


}
