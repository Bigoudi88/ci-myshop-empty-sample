package com.ci.myShop.model;

public class Book extends Item {

	
	private int nbPage ;
	private String author ;
	private String publisher;
	private int year ;
	private int age;
	
	
	public Book(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher, int year,
			int age) {
		super(name, id, price, nbrElt);
		this.nbPage = nbPage;
		this.author = author;
		this.publisher = publisher;
		this.year = year;
		this.age = age;
	}


	public int getNbPage() {
		return nbPage;
	}


	public void setNbPage(int nbPage) {
		this.nbPage = nbPage;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getPublisher() {
		return publisher;
	}


	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}



}
