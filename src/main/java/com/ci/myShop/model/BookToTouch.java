package com.ci.myShop.model;

public class BookToTouch extends Book{

	private String material;
	
	private int durability;

	public BookToTouch(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, String material, int durability) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.material = material;
		this.durability = durability;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	
	
	
}
