package com.ci.myShop.model;

public class Consumable extends Item {
	
	private int quantity = 1;
	
	public Consumable(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt);
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
}
