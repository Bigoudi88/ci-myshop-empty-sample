package com.ci.myShop.model;

import com.ci.myShop.common.Const;

public class Item {
	private String name;
	private int id;
	private float price;
	private int nbrElt;
	
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		 if(Const.PRICE_BOUND_MAX<price){
	            this.price=Const.PRICE_BOUND_MAX;
	            return;
	        }

	        if(Const.PRICE_BOUND_MIN>price) {
	            this.price = Const.PRICE_BOUND_MIN;
	            return;
	        }
    		
		this.price = price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", id=" + id + ", price=" + price + ", nbrElt=" + nbrElt + "]";
	}

	public void setNbrElt(int nbrElt) {
		
		
		 if(Const.NBELEMENT_BOUND_MAX<nbrElt){
	            this.nbrElt=Const.NBELEMENT_BOUND_MAX;
	            return;
	        }

	        if(Const.NBELEMENT_BOUND_MIN>nbrElt) {
	            this.nbrElt = Const.NBELEMENT_BOUND_MIN;
	            return;
	        }
 		
		
		this.nbrElt = nbrElt;
	}



}
