package com.ci.myShop.model;

import java.util.List;

public class MusicalBook extends Book{

	private List<String> listOfSound ;
	
	private int lifetime;
	
	private int nbrBattery;

	public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, List<String> listOfSound, int lifetime, int nbrBattery) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.listOfSound = listOfSound;
		this.lifetime = lifetime;
		this.nbrBattery = nbrBattery;
	}

	public List<String> getListOfSound() {
		return listOfSound;
	}

	public void setListOfSound(List<String> listOfSound) {
		this.listOfSound = listOfSound;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

	public int getNbrBattery() {
		return nbrBattery;
	}

	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}
	
	
}
