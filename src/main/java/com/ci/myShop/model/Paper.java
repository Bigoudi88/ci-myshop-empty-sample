package com.ci.myShop.model;

public class Paper extends Consumable{
	
	

	private enum ChoiceQuality {
		LOW,
		MEDIUM,
		HEIGH;
	}
	
	private String quality;
	
	private float weight;

	public Paper(String name, int id, float price, int nbrElt, int quantity, ChoiceQuality quality, float weight) {
		super(name, id, price, nbrElt, quantity);
		this.weight = weight;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	
	
	
	

}
