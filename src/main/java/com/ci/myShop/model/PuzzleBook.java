package com.ci.myShop.model;

public class PuzzleBook extends Book{

	 private int nbrPieces ;

	public PuzzleBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, int nbrPieces) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.nbrPieces = nbrPieces;
	}

	public int getNbrPieces() {
		return nbrPieces;
	}

	public void setNbrPieces(int nbrPieces) {
		this.nbrPieces = nbrPieces;
	}
	
	
	
}
