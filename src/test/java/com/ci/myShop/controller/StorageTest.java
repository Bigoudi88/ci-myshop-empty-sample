package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {
	private Storage storageTest;


  @Test
  public void createShopStore(){
      this.storageTest=new Storage();
      for(int i=0;i<50;i++){
          Item item1=new Item("nameTest" +i,  i, i*2, i*3);
          this.storageTest.addItem(item1);
      }
      
  }
  
  
 
  @Test
  public void addItem() {
	  this.storageTest=new Storage();
	  Item item1=new Item("Test" , 5, 100f, 3);
	  this.storageTest.addItem(item1);
	  assertEquals(1,  storageTest.getItemMap().values().size());
  }

  @Test
  public void getItem() {
	  this.storageTest=new Storage();
	  Item item1=new Item("Test" ,  5, 100f, 3);
	  this.storageTest.addItem(item1);
	  Item extractedItem = storageTest.getItem("Test");
	  assertEquals(extractedItem, item1 );

  }

}
