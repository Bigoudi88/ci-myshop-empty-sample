package com.ci.myShop.model;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.common.Const;
import com.ci.myShop.model.Item;

public class ItemTest {

	
	protected Item itemTest;
	
	 @Before
	    public void setUp() {
		 itemTest = new Item("testItem", 0, 10, 4);
	    }

	 @After
	    public void tearDown() {
	    }

	 @Test
	    public void checkPriceBoundTest(){
	        this.itemTest.setPrice(Const.PRICE_BOUND_MAX);
	        assertTrue(this.itemTest.getPrice()<=Const.PRICE_BOUND_MAX);

	        this.itemTest.setPrice(Const.PRICE_BOUND_MIN);
	        assertTrue(this.itemTest.getPrice()>=Const.PRICE_BOUND_MIN);
	    }

	 @Test
	    public void checkNbElementeBoundTest(){
	        this.itemTest.setNbrElt(Const.NBELEMENT_BOUND_MAX);
	        assertTrue(this.itemTest.getNbrElt()<=Const.NBELEMENT_BOUND_MAX);

	        this.itemTest.setNbrElt(Const.NBELEMENT_BOUND_MIN);
	        assertTrue(this.itemTest.getNbrElt()>=Const.NBELEMENT_BOUND_MIN);
	    }
	 @Test
	    public void simpleCreation(){
	        assertTrue(this.itemTest != null);
	    }

	 
}